import React from 'react'

interface Props {
    todo: Todo;
}

const TodoListItem: React.FC<Props> = ({todo}) => {
    return (
        <li className="list-group-item w-50">
            <label 
                style={{textDecoration: todo.completed ? "line-through" : undefined}}
            >
                <input id={todo.id} type="checkbox" checked={todo.completed} />{todo.title}
            </label>
        </li>
    )
}

export default TodoListItem;
