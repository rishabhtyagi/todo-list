import React, {useState, ChangeEvent, MouseEvent} from 'react'
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import InputGroup from 'react-bootstrap/InputGroup';
interface Props {
    addTodo: AddTodo;
}

const AddTodoForm: React.FC<Props> = ({ addTodo }) => {
    const [text, setText] = useState('');

    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setText(event.target.value)
      };
    const onButtonClick = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setText("");
        addTodo(text)
    } 
   
    return (
        <InputGroup className="m-3 w-25">
            <FormControl
             placeholder="Enter task description"
             onChange={onInputChange}
             value={text}
             />
             <Button variant="outline-primary" id="button" onClick={onButtonClick} disabled={text ? false : true}>
                 Button
            </Button>
        </InputGroup>
    )
}

export default AddTodoForm;
