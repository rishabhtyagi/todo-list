import React, {useEffect, useState, MouseEvent } from 'react';
import axios from 'axios';
import TodoListItem from './components/TodoListItem';
import AddTodoForm from './components/AddTodoForm';
import 'bootstrap/dist/css/bootstrap.min.css';
const initialTodos: Todo[] = [
];

function App() {

  const [todos, setTodos] = useState(initialTodos);

  const generateKey = (pre: string) => {
    return `${ pre }_${ new Date().getTime() }`;
}

  const onClick = (event:MouseEvent)=> {
    const selectedItemId = (event.target as Element).id;
    const tempTodos = todos.map((todo)=>{
      if(todo.id == selectedItemId){
        todo.completed = !todo.completed;
      }
      return todo;
    })
    setTodos(tempTodos);
  };

  const addTodo: AddTodo = (text: string): void => {
    const tempId = generateKey(text);
    const newTodo = { title: text, completed: false, id: tempId };
    setTodos([...todos, newTodo]);
  };


  useEffect(()=>{
    axios.get('https://jsonplaceholder.typicode.com/todos')
    .then(function (response) {
      setTodos(response.data)
    })
    .catch(function (error) {
      console.log(error);
    })
  
  },[])

  return <>
      <AddTodoForm addTodo={addTodo}/>
      <ul className="list-group" onClick={onClick}>
        {
          todos.map((todo)=><TodoListItem key={todo.id} todo={todo} />)
        }
    </ul>
  </>
}

export default App;
