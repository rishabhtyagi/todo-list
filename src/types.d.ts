interface Todo {
    title: string;
    completed: boolean;
    id: string;
  }

  type AddTodo = (text: string) => void;